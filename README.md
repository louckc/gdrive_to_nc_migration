
This code syncronize a google drive to nextcloud and changes the hyperlinks in the google drive to point to the same document in nextcloud. It covers document in format .docx and .xlsx.
 
The code performe the drive syncronisation and link changing in the same script because it feeted our need but the two part are actually independent.

## Prerequisite
-Rclone :
    https://rclone.org/downloads/

-Configured remote for Google drive and Nextcloud


## Google Drive Specificities

### Duplicates files
Google drive allows for duplicate file names in the same folder this is a specificity that ohers drive as Nextcloud does not allows. Those duplicates files while cause errors during the syncronisation and the link changing script so they have to be delt with before. Hopefully rclone provides a tool for that : 
https://rclone.org/commands/rclone_dedupe/

### Dangling shortcuts
Shortcuts are a specials features of google drive that act as sylinks. Normaly those files are transfered by rclone. 
A dangling shortcuts occurs when the shortcut point to a file that has been deleted or to which the users having created the rclone remote has not been granted access.
The dangling shortcuts are extracted for the sync log and the errors due to them ignored

### --drive-shared-with-me
This options is used to tell rclone to search in the "Shared with me" folder in Google drive. This highly depends on the organisation of your drive and have to be added/removed accordlingly. Note that if this options is set, rclone will only look up in the "Shared with me" folder and not in the other folder you created.