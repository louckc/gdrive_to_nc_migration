from docx import Document
from docx.opc.constants import RELATIONSHIP_TYPE as RT
from openpyxl import load_workbook

import subprocess
import json
import nextcloud_client
import logging
import re

NC_URL = 'https://nuagenextcloud.cooperative.coop'  # Base url of nextcloud drive
NC_USER_NAME = 'username'  # Nexctould username
NC_PASSWORD_FILE = 'nc_pass.txt'  # File containing Nextcloud's password
REMOTE_GDRIVE = 'gDrive:'  # Name of the google drive remote
REMOTE_NC = 'nc:'  # Name of the nextcloud remote
WORKING_FOLDER = r""  # Folder you want to transfer, leave empty if you want to transfer
                      # whole drive
SYNC_FILTER_FILE = 'sync_filter.txt'  # File containing filter rules for rclone
SYNC_LOG = "sync_stderr.txt"  # Log file for rclone sync
STATS = {
    'link_changed': 0,
    'link_unchanged': 0,
    'unchanged_because_dangling': 0,
}

# Clear SYNC_LOG
f = open(SYNC_LOG, "w")
f.close()

logging.basicConfig(filename="log_file.log", level=logging.INFO)


def get_gdrive_files_ID(remote_name, directory=""):

    ''' From a rclone remote_name and a starting directory loop recursively to
       return a dictionary containg the path to the file and it's google
       file_ID (the file hash)

            Inputs:
                (str) remote_name: name of the remote (Ex: "mydrive:")
                (str) directory (OPTIONAL): starting directory

            Output:
                (dict) id_dict: dictionary of the form {
                                                         file_path:
                                                            { id: "file_id",
                                                              isDir: True|False
                                                            }
                                                        }
    '''

    id_dict = {}
    remote_dest = remote_name + directory
    completed_process = subprocess.run(
        ["rclone", "lsjson", remote_dest, "--drive-shared-with-me"],
        capture_output=True
    )

    files = completed_process.stdout.decode()
    sub_folder_list = []
    for f in json.loads(files):
        f_full_path = directory + '/' + f["Path"]
        if f["IsDir"]:
            sub_folder_list.append(f_full_path)

        id_dict[f_full_path] = {'id': f["ID"], 'isDir': f["IsDir"]}

    for sub_folder in sub_folder_list:
        sf_dict = get_gdrive_files_ID(remote_name, sub_folder)
        id_dict.update(sf_dict)
    return id_dict


def get_nc_files_ID(files_paths, nc_url, nc_user, nc_pass_file, base_dir):
    ''' From a list of file path question the nextcloud api to get the
        nextcloud id of the file.

            Inputs:
                ([str]) files_paths: list of file path
                (str) nc_url: url of the nextcloud instance
                (str) nc_user: login for the nextcloud instance
                (str) nc_pass_file: path to the file containing the password
                (str) base_dir: base directory if file if file paths are not absolute

            Outputs:
                (dir) id_dict: Directory of the form
                                    { file_full_path: nextcloud id}
    '''

    id_dict = {}
    nc = nextcloud_client.Client(nc_url)
    with open(nc_pass_file, 'r') as pass_file:
        nc.login = nc.login(nc_user, pass_file.read().rstrip())

    api_id_arg = "{http://owncloud.org/ns}fileid"
    for f_path in files_paths:
        file_full_path = base_dir + f_path
        try:
            file_info = nc.file_info(file_full_path, [api_id_arg])
            id_dict[file_full_path] = file_info.attributes[api_id_arg]
        except nextcloud_client.nextcloud_client.HTTPResponseError:
            print(f'Could not find the file {file_full_path} in nc drive')
    return id_dict


def merge_id_dict(g_id_dict, nc_id_dict):
    ''' Merge the two ids dict to obtain a direct
        gdrive_id/nc_id correspondance.

            Inputs:
                (dict) g_id_dict: dict of the form {filepath: {gdrive_id,
                                                                 isDir}
                                                                 }
                (dict) nc_id_dict: dict of the form {filepath: nc_id}

            Output:
                (dict) ids_dict: dict of the form {gdrive_id: {nc_id,
                                                                 isDir}
                                                                 }
    '''
    dangling_shortcuts = extract_dangling_shortcuts_from_log()
    print(dangling_shortcuts)
    ids_dict = {}
    for key in g_id_dict.keys():
        try:
            ids_dict[g_id_dict[key]['id']] = {'nc_id': nc_id_dict[key],
                                              'isDir': g_id_dict[key]['isDir']
                                              }
        except KeyError as e:
            print('While merging dict obtain a KeyError with key: ', e.args[0])
            if dangling_shortcuts.count(e.args[0]) > 0:
                print('The error is due to a dangling shortcut')
    return ids_dict


def get_google_id_from_url(link_url, isDir):
    '''  Split the url of a google hyperlink to extract the google id '''
    if 'open?id=' in link_url:
        g_id = re.findall(r'open\?id=([^\?#/]*)', link_url)[0]

    else:
        if isDir:
            g_id = re.findall(r'/folders/([^/\?]*)\??', link_url)[0]

        else:
            g_id = re.findall(r'/d/([^\?#/]*)', link_url)[0]

    return g_id


def generate_nc_link_from_id(nc_id, isDir):
    ''' Generate the url from a nextcloud hyperlink from nextcloud id '''
    if isDir:
        nc_link = 'https://nuage.commown.coop/f/' + nc_id
    else:
        nc_link = 'https://nuage.commown.coop/apps/onlyoffice/' + nc_id

    return nc_link


def detect_dir_from_gdrive_link(url):
    ''' Return True if the url of the hyperlink point to a directory '''
    return 'folder' in url


def gdrive_link_to_nc_link(gdrive_link, dict_ids):
    ''' Change a google drive hyperlink url to a nextcloud hyperlink url '''
    print('Changing link ', gdrive_link)
    isDir = detect_dir_from_gdrive_link(gdrive_link)
    print('This link isDir:', isDir)
    gdrive_id = get_google_id_from_url(gdrive_link, isDir)
    print('Detected gdrive_id for this link is:', gdrive_id)
    try:
        nc_link = generate_nc_link_from_id(dict_ids[gdrive_id]['nc_id'], isDir)
        STATS['link_changed'] += 1

    except KeyError:
        print(
            f'KeyError with link {gdrive_link} wile'
            f'looking id ids_dict\nNot Changing the Link'
        )

        logging.error(
            '''\n\nError with link: %s\n No matching google drive id\n''',
            gdrive_link
        )
        nc_link = gdrive_link
        STATS['link_unchanged'] += 1
    return nc_link


def docx_change_hyperlink(file_name, dict_ids):
    '''Change google hyperlinks to nexctloud hyperlink in docx files'''
    document = Document(file_name)
    rels = document.part.rels

    for rel in rels:
        if (rels[rel].reltype == RT.HYPERLINK
            and rels[rel]._target.startswith(
                ("https://docs.google.com", "https://drive.google.com/")
             )):
            new_url = gdrive_link_to_nc_link(rels[rel]._target, dict_ids)

            logging.info(
                '''\nChanging link: %s\nBy: %s\nIn: %s''',
                rels[rel]._target,
                new_url,
                file_name,
            )

            rels[rel]._target = new_url
    document.save(file_name)


def xslx_change_hyperlink(file_name, dict_ids):
    '''Change google hyperlinks to nexctloud hyperlink in xlxs files'''
    wb = load_workbook(file_name)
    for sheetname in wb.sheetnames:
        ws = wb[sheetname]
        for r in ws.rows:
            for c in r:
                if (c.hyperlink is not None
                and c.hyperlink.target.startswith(
                    ("https://docs.google.com", "https://drive.google.com/")
                )):
                    new_url = gdrive_link_to_nc_link(
                        c.hyperlink.target,
                        dict_ids,
                    )

                    logging.info(
                        '''\nChanging link: %s\nBy: %s\nIn: %s''',
                        c.hyperlink.target,
                        new_url,
                        file_name
                    )
                    c.hyperlink.target = new_url
    wb.save(file_name)
    wb.close()


def change_hyperlinks(files_list, dict_ids, gdrive_ids):
    for f in files_list:
        if f[-4:] == 'docx':
            docx_change_hyperlink(f, dict_ids)

        elif f[-4:] == 'xlsx':
            xslx_change_hyperlink(f, dict_ids)


def extract_dangling_shortcuts_from_log():
    with open(SYNC_LOG, "r") as log_file:
        dangling_shortcuts = []
        for line in log_file.readlines():
            if "ERROR" in line and "dangling shortcut" in line:
                filename = re.findall(r'ERROR: (.*?):', line)[0]
                if not re.search('Attempt \d/\d failed with', filename):
                    dangling_shortcuts.append(WORKING_FOLDER + '/' + filename)

        cleaned_shortcuts_list = list(set(dangling_shortcuts))
        print(len(cleaned_shortcuts_list))
        print(cleaned_shortcuts_list)
        return cleaned_shortcuts_list


def sync_drive_to_other_drive(src_remote, dest_remote, folder=""):
    with open(SYNC_LOG, "w") as stderr_file:
        subprocess.run(
            [
                "rclone",
                "sync",
                src_remote + folder,
                dest_remote + folder,
                "--create-empty-src-dirs",
                "--drive-shared-with-me",
                "--progress"
            ],
            # "--drive-copy-shortcut-content"],
            stdout=stderr_file
        )


def sync_remote_to_local(remote_name, folder=""):
    '''Syncronize distant drive to local using filter to synchronize only
       the files delt by the change_hyperlinks function.
    '''
    subprocess.run(
        [
            "rclone",
            "sync",
            remote_name + folder,
            "./tmp/" + folder,
            "--filter-from",
            SYNC_FILTER_FILE,
            "--progress"
        ]
    )


def sync_local_to_remote(remote_name, folder=""):
    subprocess.run(["rclone",
                    "sync",
                    "./tmp/" + folder,
                    remote_name + folder,
                    "--filter-from",
                    SYNC_FILTER_FILE,
                    "--progress"])


def change_file_path_list_to_local(files_list):
    return ['./tmp/' + f for f in files_list]


if __name__ == "__main__":
    print('Synchronizing google to nextcloud')
    sync_drive_to_other_drive(REMOTE_GDRIVE, REMOTE_NC, WORKING_FOLDER)
    print("Getting google ids...")
    gdrive_ids = get_gdrive_files_ID(REMOTE_GDRIVE, WORKING_FOLDER)
    files_list = gdrive_ids.keys()
    print("Getting nextcloud ids...")
    nc_ids = get_nc_files_ID(
        files_list,
        NC_URL,
        NC_USER_NAME,
        NC_PASSWORD_FILE,
        "",
    )
    ids_dict = merge_id_dict(gdrive_ids, nc_ids)
    print('Synchronizing nextcloud to local')
    sync_remote_to_local(REMOTE_NC, WORKING_FOLDER)
    local_files_list = change_file_path_list_to_local(files_list)
    print('Changing hyperlinks...')
    change_hyperlinks(local_files_list, ids_dict, gdrive_ids)
    print('Synchronizing local to nextcloud')
    sync_local_to_remote(REMOTE_NC, WORKING_FOLDER)
    subprocess.run(["rm", "-rf", "tmp/"])

    for stat in STATS.keys():
        print(f"{stat}: {STATS[stat]}")
